public class BankAccount implements Measurable {
	private String name;
	private double balance;
	
	public BankAccount(String name,double balance){
		this.name = name;
		this.balance = balance;
	}

	public String getName() {
		return name;
	}

	public double getBalance() {
		return balance;
	}

	@Override
	public double getMeasure() {
		return balance;
	}
	
	
	
	
	

}
