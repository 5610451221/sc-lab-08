public class Company implements Taxable {
	
	private String name;
	private double income;
	private double expenses;
	
	public Company(String name,double income,double expenses){
		this.name = name;
		this.income = income;
		this.expenses = expenses;
	}

	
	public String getName() {
		return name;
	}


	public double getIncome() {
		return income;
	}


	public double getExpenses() {
		return expenses;
	}



	@Override
	public double getTax() {
		double tax =0;
		tax = ((income-expenses)*30)/100;
		return tax;
	}
	
	
}
