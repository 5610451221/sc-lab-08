import java.time.chrono.MinguoChronology;
import java.util.ArrayList;



public class Testser {

	public static void main(String[] args) {
		
//Average Height -------------------------------------------------------------		
		Measurable[] people = new Measurable[3];
		people[0] = new Person("May",170);
		people[1] = new Person("Ployphuak",180);
		people[2] = new Person("Biew",155);
		
		double averageHeight = Data.average(people);
		System.out.println("Average height:"+" "+averageHeight);
		System.out.println("------------------------------------------------------");
		

//Min -------------------------------------------------------------------------			
		BankAccount bank1 = new BankAccount("May",20000);
		BankAccount bank2 = new BankAccount("Ployphuak",10000);
		Measurable mini1 = Data.min(bank1, bank2);
		BankAccount minAccount = (BankAccount) mini1;
		String name1 = minAccount.getName();
		Double balance = minAccount.getBalance();
		System.out.println("Min Account:"+ " "+ name1 +" " +"Balance:" + " " + balance );
		
		Country uruguay = new Country("Uraguay", 176220);
		Country thailand = new Country("Thailand", 514000);
		Measurable mini2 = Data.min(uruguay, thailand);
		Country minCountry = (Country) mini2;
		String name2 = minCountry.getName();
		Double area = minCountry.getArea();
		System.out.println("Min Country:"+ " "+ name2+" "+ "Area:" + " " + area);
		
		Person person1 = new Person("May",170);
		Person person2 = new Person("Ployphuak",180);
		Measurable mini3 = Data.min(person1, person2);
		Person minPerson = (Person) mini3;
		String name3 = minPerson.getName();
		Double height = minPerson.getheight();
		System.out.println("Min Person:"+ " "+ name3+" "+"Height:" + " "+ height);
		System.out.println("------------------------------------------------------");

		
//Average Min -----------------------------------------------------------------	
//		Measurable[] total = new Measurable[3];
//		total[0] = minAccount;
//		total[1] = minCountry;
//		total[2] = minPerson;
//		double averageTotal = Data.average(total);
//		System.out.println("Total Average:"+" "+averageTotal);
		
		
		
//Tax --------------------------------------------------------------------------
		
		
		Person person3 = new Person("May",160,600000);
		Person person4 = new Person("PloyPhuak,",170,5000);
		
		Company com1 = new Company("May", 500, 100);
		Company com2 = new Company("PloyPhuak",300,700);
		
		
		Product pro1 = new Product("DookDik",550);
		Product pro2 = new Product("Pet",800);
		
		ArrayList<Taxable> taxable = new ArrayList<Taxable>();
		taxable.add(person3);
		taxable.add(person4);
		taxable.add(com1);
		taxable.add(com2);
		taxable.add(pro1);
		taxable.add(pro2);
		
		System.out.println("TaxCalculator" + " " +TaxCalculator.sum(taxable));
	
	}
}
