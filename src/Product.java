public class Product implements Taxable {
	
	private String name;
	private double price;
	
	public Product(String name,double price){
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public double getPrice() {
		return price;
	}

	@Override
	public double getTax() {
		double tax =0;
		tax = (price*7)/100;
		return tax;
	}
	
	

}
