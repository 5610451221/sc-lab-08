public class Person implements Measurable,Taxable {
	private String name;
	private double height;
	private double earnings;
	private Person getTax;
	
	public Person(String name,double height){
		this.name = name;
		this.height = height;
	}
	
	public Person(String name,double height , double earings){
		this.name = name;
		this.height = height;
		this.earnings = earings;
	}
	
	public String getName() {
		return name;
	}

	public double getheight() {
		return height;
	}
	
	public double getEarnings(){
		return earnings;
	}

	
	@Override
	public double getMeasure() {
		return height;
	}

	@Override
	public double getTax() {
		double tax = 0;
		if(earnings <= 300000){
			tax = (earnings*5)/100;
		}
		else{
			double total = earnings - 300000;
			double tax1 = (30000*5)/100;
			double tax2 = (earnings*10)/100;			
			tax = tax1+tax2; 
		}
		return tax;
	}

}
